package org.igaft.homeorganizer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MainActivity";
    private final boolean debug_mode = false;

    private final int requestCodePermissionCameraAndStorage = 0;
    private final int requestCodeTakePictureResult = 1;

    private static final String FILE_NAME_MEDIA_STORAGE_DIR = "HomeOrganizer";

    private Button btnSnap;
    private DrawView imgViewSnap;
    private Paint imgViewSnapPaint;
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initInstances();

        // Disable btnSnap if no permissions granted to use camera and request permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            btnSnap.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, requestCodePermissionCameraAndStorage);
        }

        if(debug_mode){
            imageUri = FileProvider.getUriForFile(this
                    ,  BuildConfig.APPLICATION_ID + ".org.igaft.homeorganizer.provider"
                    , new File(getMediaStorageDir().getPath() + File.separator +
                            "IMG_20171229_223825.jpg")
            );
            showPicture();
        }
    }

    private void initInstances(){
        btnSnap = this.findViewById(R.id.mainBtnSnap);
        imgViewSnap = this.findViewById(R.id.mainImgViewSnap);

        btnSnap.setOnClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == requestCodePermissionCameraAndStorage) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                btnSnap.setEnabled(true);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(btnSnap.equals(view)){
            takePicture();
        }
    }

    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(this,  BuildConfig.APPLICATION_ID + ".org.igaft.homeorganizer.provider", getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivityForResult(intent, requestCodeTakePictureResult);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCodeTakePictureResult && resultCode == RESULT_OK) {
            showPicture();
        }
    }

    private void showPicture(){
        try {
            Bitmap d = MediaStore.Images.Media.getBitmap(getContentResolver() , imageUri);
            int nh = (int) ( d.getHeight() * (2048.0 / d.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(d, 2048, nh, true);
            imgViewSnap.setImageBitmap(scaled);
        } catch (IOException e) {
            Log.e("showPicture", "showPicture: Error loading picture taken", e);
        }
    }

    private File getOutputMediaFile(){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(getMediaStorageDir().getPath()+File.separator+"IMG_"+timeStamp+".jpg");
    }

    private File getMediaStorageDir(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), FILE_NAME_MEDIA_STORAGE_DIR);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        return mediaStorageDir;
    }
}
