package org.igaft.homeorganizer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.ThumbnailUtils;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.HashSet;
import java.util.Set;

class DrawView extends AppCompatImageView implements View.OnTouchListener {

    private final String TAG = "DrawView";

    private Paint paint;
    private Rectangle currentRectangle;
    private Bitmap bitmap;
    private Paint bitmapPaint;
    private Canvas canvas;

    private Set<Rectangle> rectangles = new HashSet<>();

    private boolean touchMoved = false;

    private class Rectangle{
        float x1,x2,y1,y2;
        Rectangle(float x1,float y1){this.x1=x1;this.y1=y1;this.x2=0;this.y2=0;}
        Rectangle(float x1,float y1,float x2,float y2){this.x1=x1;this.y1=y1;this.x2=x2;this.y2=y2;}
    }

    public DrawView(Context context) {
        super(context);
    }

    DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    DrawView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setPaint(Paint paint){
        this.paint = paint;
    }

    private void initFields(){
//        Log.i(TAG,"initFields: start");
        setOnTouchListener(this);
        setDrawingCacheEnabled(true);
        if(currentRectangle == null){
            currentRectangle = new Rectangle(0,0);
        }

        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(10);
        paint.setStyle(Paint.Style.STROKE);

        bitmapPaint = new Paint();
    }

    public void touchDown(float x, float y){
        setRectangleStart(x,y);
        setDrawingCacheEnabled(true);
    }

    public void touchMove(float x, float y){
        setRectangleEnd(x,y);
        touchMoved = true;
        invalidate();
    }

    public void touchUp(float x, float y){
        if(touchMoved) {
            setRectangleEnd(x,y);
            addRectangleToSet();
            bitmap = getDrawingCache();
            bitmap = ThumbnailUtils.extractThumbnail(bitmap, getWidth(), getHeight());
            setImageBitmap(bitmap);
            touchMoved = false;
            invalidate();
            setDrawingCacheEnabled(false);
        }
    }

    private void setRectangleStart(float x1,float y1){
        currentRectangle = new Rectangle(x1,y1);
    }

    private void setRectangleEnd(float x2, float y2){
        //TODO: check if rectangle overlaps with rectangles in set, if so, find smallest non-overlapping area
        currentRectangle.x2 = x2;
        currentRectangle.y2 = y2;
    }

    private void addRectangleToSet() {
        //TODO: Implement method
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initFields();
        Rectangle r = currentRectangle;
        if(bitmap != null) canvas.drawBitmap(bitmap,0,0,bitmapPaint);
        canvas.drawRect(r.x1, r.y1, r.x2, r.y2, paint);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            touchDown(motionEvent.getX(),motionEvent.getY());
        }else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE){
            touchMove(motionEvent.getX(),motionEvent.getY());
        }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
            touchUp(motionEvent.getX(),motionEvent.getY());
        }else{
            return false;
        }
        return true;
    }
}